<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'ApiController@eventslist');
Route::get('/events', 'ApiController@eventslist');
Route::post('/events', 'ApiController@eventscreate');
Route::post('/eventedit', 'ApiController@eventsedit');

Route::get('/people', 'ApiController@peoplelist');
Route::post('/people', 'ApiController@peoplecreate');
Route::post('/peopleedit', 'ApiController@peopleedit');
Route::post('/peopledelete', 'ApiController@peopledelete');
