@extends('layouts.app')
@section('content')
<div class="table-wrapper">
  <div class="table-title">
    <div class="row">
      <div class="col-sm-6">
        <h2 class="font22">Manage <b>People</b></h2>
      </div>
      <div class="col-sm-6">
        <a href="#addPeopleModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New People</span></a>
      </div>
    </div>
  </div>
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>
          <span class="custom-checkbox">
          </span>
        </th>
        <th>Name</th>
        <th>Email</th>
        <th>Gender</th>
        <th>Employer</th>
        <th>Address</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($data['results']) && count($data['results'])>0){
        foreach($data['results'] as $k=>$v){
      ?>
      <tr>
        <td>
          <span class="custom-checkbox">
          </span>
        </td>
        <td><?php echo !empty($v['first_name'])?$v['first_name']." ".$v['last_name']:""; ?></td>
        <td><?php echo !empty($v['email'])?$v['email']:""; ?></td>
        <td><?php echo !empty($v['sex'])?$v['sex']:""; ?></td>
        <td><?php echo !empty($v['employer'])?$v['employer']:""; ?></td>
        <td><?php echo !empty($v['primary_address']['state'])?$v['primary_address']['state']." ".$v['primary_address']['country_code']:""; ?></td>
        <td>
          <a href="#editPeopleModal" class="edit editShowData" data-attr='<?php echo json_encode($v,true); ?>' data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
          <a href="#deletePeopleModal" class="delete deleteShowData" data-attr='<?php echo json_encode($v,true); ?>' data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
        </td>
      </tr>
      <?php
          }
        }
      ?>
    </tbody>
  </table>
  <div class="clearfix">
    <div class="hint-text">Showing <b><?php if(isset($data['results'])){ echo count($data['results']); }else{ echo '0'; } ?></b>
      entries</div>
  </div>
</div>
<script>
$(function () {

        $('a.deleteShowData').on('click', function (e) {
          var editData = $(this).attr('data-attr');
          editData = JSON.parse(editData);
          console.log(editData);
          $('form#deletePeopleForm .peopleId').attr('value',editData.id);
        });

        $('form#deletePeopleForm').on('submit', function (e) {
          $('.preload').show();
          e.preventDefault();
          console.log($('form').serialize());//return false;
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'post',
            url: '<?php echo url('/'); ?>/peopledelete',
            data: $('form#deletePeopleForm').serialize(),
            success: function (response) {
              $('.preload').fadeOut();
              if(response.success==false){
                swal("Error!", response.message, "error");
                errormsg = '<div class="alert alert-danger">';
                for (var key in response.data) {
                    errormsg += response.data[key];
                    if(key==response.data.length-2){
                      errormsg += '<br>';
                    }
                }
                errormsg += '</div>';
                $('.errormessages').html(errormsg);
              }else{
                swal({
                    title: "Good job!",
                    text: response.message,
                    icon: "success"
                })
                .then((isOK) => {
                  if (isOK) {
                    window.location.href = "<?php url('/people'); ?>";
                  }
                });
                $('#deletePeopleModal').modal('hide');
              }
            }
          });

        });

        $('a.editShowData').on('click', function (e) {
          var editData = $(this).attr('data-attr');
          editData = JSON.parse(editData);
          console.log(editData);
          $('form#editPeopleForm .peopleId').attr('value',editData.id);
          $('form#editPeopleForm .firstName').attr('value',editData.first_name);
          $('form#editPeopleForm .lastName').attr('value',editData.last_name);
          $('form#editPeopleForm .email').attr('value',editData.email);
          $('form#editPeopleForm .sex').attr('value',editData.sex);
          $('form#editPeopleForm .employer').attr('value',editData.employer);
          $('form#editPeopleForm .registeredAddressState').find('option[value="'+editData.primary_address.state+'"]').attr("selected",true);
          $('form#editPeopleForm .registeredAddressState').val(editData.primary_address.state);
        });
        $('form#addPeopleForm').on('submit', function (e) {
          $('.preload').show();
          e.preventDefault();
          console.log($('form').serialize());//return false;
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'post',
            url: '<?php echo url('/'); ?>/people',
            data: $('form#addPeopleForm').serialize(),
            success: function (response) {
              $('.preload').fadeOut();
              if(response.success==false){
                swal("Error!", response.message, "error");
                errormsg = '<div class="alert alert-danger">';
                for (var key in response.data) {
                    errormsg += response.data[key];
                    if(key==response.data.length-2){
                      errormsg += '<br>';
                    }
                }
                errormsg += '</div>';
                $('.errormessages').html(errormsg);
              }else{
                swal({
                    title: "Good job!",
                    text: response.message,
                    icon: "success"
                })
                .then((isOK) => {
                  if (isOK) {
                    window.location.href = "<?php url('/people'); ?>";
                  }
                });
                $('#addPeopleModal').modal('hide');
              }
            }
          });

        });

        $('form#editPeopleForm').on('submit', function (e) {
          $('.preload').show();
          var eventid = $('form#editPeopleForm .eventId').val();
          e.preventDefault();
          console.log($('form').serialize());//return false;
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'post',
            url: '<?php echo url('/'); ?>/peopleedit',
            data: $('form#editPeopleForm').serialize(),
            success: function (response) {
              $('.preload').fadeOut();
              if(response.success==false){
                swal("Error!", response.message, "error");
                errormsg = '<div class="alert alert-danger">';
                for (var key in response.data) {
                    errormsg += response.data[key];
                    if(key==response.data.length-2){
                      errormsg += '<br>';
                    }
                }
                errormsg += '</div>';
                $('.errormessages').html(errormsg);
              }else{
                swal({
                    title: "Good job!",
                    text: response.message,
                    icon: "success"
                })
                .then((isOK) => {
                  if (isOK) {
                    window.location.href = "<?php url('/people'); ?>";
                  }
                });
                $('#editPeopleModal').modal('hide');
              }
            }
          });

        });

      });
</script>
@endsection
