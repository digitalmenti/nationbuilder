@extends('layouts.app')
@section('content')
<div class="table-wrapper">
  <div class="table-title">
    <div class="row">
      <div class="col-sm-6">
        <h2 class="font22">Manage <b>Events</b></h2>
      </div>
      <div class="col-sm-6">
        <a href="#addEventModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Event</span></a>
      </div>
    </div>
  </div>
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>
          <span class="custom-checkbox">
          </span>
        </th>
        <th>Event name</th>
        <th>Contact name</th>
        <th>Contact email</th>
        <th>Capacity</th>
        <th>Venue</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($data['results']) && count($data['results'])>0){
        foreach($data['results'] as $k=>$v){
          $v['start_time'] = date('Y-m-d',strtotime($v['start_time']));
          $v['end_time'] = date('Y-m-d',strtotime($v['end_time']));
          $v['show_guests'] = ($v['show_guests']==1)?true:false;
      ?>
      <tr>
        <td>
          <span class="custom-checkbox">
          </span>
        </td>
        <td><?php echo !empty($v['name'])?$v['name']:""; ?></td>
        <td><?php echo !empty($v['contact']['name'])?$v['contact']['name']:""; ?></td>
        <td><?php echo !empty($v['contact']['email'])?$v['contact']['email']:""; ?></td>
        <td><?php echo !empty($v['capacity'])?$v['capacity']:""; ?></td>
        <td><?php echo !empty($v['venue']['name'])?$v['venue']['name']:""; ?></td>
        <td>
          <a href="#editEventModal" class="edit editShowData" data-attr='<?php echo json_encode($v,true); ?>' data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
        </td>
      </tr>
      <?php
          }
        }
      ?>
    </tbody>
  </table>
  <div class="clearfix">
    <div class="hint-text">Showing <b><?php if(isset($data['results'])){ echo count($data['results']); }else{ echo '0'; } ?></b>
      entries</div>
  </div>
</div>
<script>
$(function () {

        $('a.editShowData').on('click', function (e) {
          var editData = $(this).attr('data-attr');
          editData = JSON.parse(editData);
          $('form#editEventForm .eventId').attr('value',editData.id);
          $('form#editEventForm input[name=name]').attr('value',editData.name);
          $('form#editEventForm input[name=intro]').attr('value',editData.intro);
          $('form#editEventForm input[name=start_time]').attr('value',editData.start_time);
          $('form#editEventForm input[name=end_time]').attr('value',editData.end_time);
          $('form#editEventForm select[name=show_guests]').find('option[value="'+editData.show_guests+'"]').attr("selected",true);
          $('form#editEventForm select[name=show_guests]').attr('value',editData.show_guests);;
          $('form#editEventForm input[name=capacity]').attr('value',editData.capacity);
          $('form#editEventForm .contactName').attr('value',editData.contact.name);
          $('form#editEventForm .contactEmail').attr('value',editData.contact.email);
          $('form#editEventForm .contactPhone').attr('value',editData.contact.phone);
          $('form#editEventForm .venueName').attr('value',editData.venue.name);
          $('form#editEventForm .venueAddressAddress1').attr('value',editData.venue.address.address1);
          $('form#editEventForm .venueAddressCity').attr('value',editData.venue.address.city);
          $('form#editEventForm .venueAddressState').find('option[value="'+editData.venue.address.state+'"]').attr("selected",true);
          $('form#editEventForm .venueAddressState').val(editData.venue.address.state);
        });
        $('form#addEventForm').on('submit', function (e) {
          $('.preload').show();
          e.preventDefault();
          console.log($('form').serialize());//return false;
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'post',
            url: '<?php echo url('/'); ?>/events',
            data: $('form#addEventForm').serialize(),
            success: function (response) {
              $('.preload').fadeOut();
              if(response.success==false){
                swal("Error!", response.message, "error");
                errormsg = '<div class="alert alert-danger">';
                for (var key in response.data) {
                    errormsg += response.data[key];
                    if(key==response.data.length-2){
                      errormsg += '<br>';
                    }
                }
                errormsg += '</div>';
                $('.errormessages').html(errormsg);
              }else{
                swal({
                    title: "Good job!",
                    text: response.message,
                    icon: "success"
                })
                .then((isOK) => {
                  if (isOK) {
                    window.location.href = "<?php url('/events'); ?>";
                  }
                });
                $('#addEventModal').modal('hide');
              }
            }
          });

        });

        $('form#editEventForm').on('submit', function (e) {
          $('.preload').show();
          var eventid = $('form#editEventForm .eventId').val();
          e.preventDefault();
          console.log($('form').serialize());//return false;
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'post',
            url: '<?php echo url('/'); ?>/eventedit',
            data: $('form#editEventForm').serialize(),
            success: function (response) {
              $('.preload').fadeOut();
              if(response.success==false){
                swal("Error!", response.message, "error");
                errormsg = '<div class="alert alert-danger">';
                for (var key in response.data) {
                    errormsg += response.data[key];
                    if(key==response.data.length-2){
                      errormsg += '<br>';
                    }
                }
                errormsg += '</div>';
                $('.errormessages').html(errormsg);
              }else{
                swal({
                    title: "Good job!",
                    text: response.message,
                    icon: "success"
                })
                .then((isOK) => {
                  if (isOK) {
                    window.location.href = "<?php url('/events'); ?>";
                  }
                });
                $('#editEventModal').modal('hide');
              }
            }
          });

        });

      });
</script>
@endsection
