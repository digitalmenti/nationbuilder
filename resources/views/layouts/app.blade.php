<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>NationBuilder API Integration</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style type="text/css">
    body {
        color: #566787;
		background: #f5f5f5;
		font-family: 'Varela Round', sans-serif;
		font-size: 13px;
	}
	.table-wrapper {
        background: #fff;
        padding: 20px 25px;
        margin: 30px 0;
		border-radius: 3px;
        box-shadow: 0 1px 1px rgba(0,0,0,.05);
    }
	.table-title {
		padding-bottom: 15px;
		/* background: #435d7d; */
		background: #922871;
		color: #fff;
		padding: 16px 30px;
		margin: -20px -25px 10px;
		border-radius: 3px 3px 0 0;
    }
    .table-title h2 {
		margin: 5px 0 0;
		font-size: 24px;
	}
	.table-title .btn-group {
		float: right;
	}
	.table-title .btn {
		color: #fff;
		float: right;
		font-size: 13px;
		border: none;
		min-width: 50px;
		border-radius: 2px;
		border: none;
		outline: none !important;
		margin-left: 10px;
	}
	.table-title .btn i {
		float: left;
		font-size: 21px;
		margin-right: 5px;
	}
	.table-title .btn span {
		float: left;
		margin-top: 2px;
	}
    table.table tr th, table.table tr td {
        border-color: #e9e9e9;
		padding: 12px 15px;
		vertical-align: middle;
    }
	table.table tr th:first-child {
		width: 60px;
	}
	table.table tr th:last-child {
		width: 100px;
	}
    table.table-striped tbody tr:nth-of-type(odd) {
    	background-color: #fcfcfc;
	}
	table.table-striped.table-hover tbody tr:hover {
		background: #f5f5f5;
	}
    table.table th i {
        font-size: 13px;
        margin: 0 5px;
        cursor: pointer;
    }
    table.table td:last-child i {
		opacity: 0.9;
		font-size: 22px;
        margin: 0 5px;
    }
	table.table td a {
		font-weight: bold;
		color: #566787;
		display: inline-block;
		text-decoration: none;
		outline: none !important;
	}
	table.table td a:hover {
		color: #2196F3;
	}
	table.table td a.edit {
        color: #FFC107;
    }
    table.table td a.delete {
        color: #F44336;
    }
    table.table td i {
        font-size: 19px;
    }
	table.table .avatar {
		border-radius: 50%;
		vertical-align: middle;
		margin-right: 10px;
	}
    .pagination {
        float: right;
        margin: 0 0 5px;
    }
    .pagination li a {
        border: none;
        font-size: 13px;
        min-width: 30px;
        min-height: 30px;
        color: #999;
        margin: 0 2px;
        line-height: 30px;
        border-radius: 2px !important;
        text-align: center;
        padding: 0 6px;
    }
    .pagination li a:hover {
        color: #666;
    }
    .pagination li.active a, .pagination li.active a.page-link {
        background: #03A9F4;
    }
    .pagination li.active a:hover {
        background: #0397d6;
    }
	.pagination li.disabled i {
        color: #ccc;
    }
    .pagination li i {
        font-size: 16px;
        padding-top: 6px
    }
    .hint-text {
        float: left;
        margin-top: 10px;
        font-size: 13px;
    }
	/* Custom checkbox */
	.custom-checkbox {
		position: relative;
	}
	.custom-checkbox input[type="checkbox"] {
		opacity: 0;
		position: absolute;
		margin: 5px 0 0 3px;
		z-index: 9;
	}
	.custom-checkbox label:before{
		width: 18px;
		height: 18px;
	}
	.custom-checkbox label:before {
		content: '';
		margin-right: 10px;
		display: inline-block;
		vertical-align: text-top;
		background: white;
		border: 1px solid #bbb;
		border-radius: 2px;
		box-sizing: border-box;
		z-index: 2;
	}
	.custom-checkbox input[type="checkbox"]:checked + label:after {
		content: '';
		position: absolute;
		left: 6px;
		top: 3px;
		width: 6px;
		height: 11px;
		border: solid #000;
		border-width: 0 3px 3px 0;
		transform: inherit;
		z-index: 3;
		transform: rotateZ(45deg);
	}
	.custom-checkbox input[type="checkbox"]:checked + label:before {
		border-color: #03A9F4;
		background: #03A9F4;
	}
	.custom-checkbox input[type="checkbox"]:checked + label:after {
		border-color: #fff;
	}
	.custom-checkbox input[type="checkbox"]:disabled + label:before {
		color: #b8b8b8;
		cursor: auto;
		box-shadow: none;
		background: #ddd;
	}
	/* Modal styles */
	.modal .modal-dialog {
		/* max-width: 600px; */
	}
	.modal .modal-header, .modal .modal-body, .modal .modal-footer {
		padding: 20px 30px;
	}
	.modal .modal-content {
		border-radius: 3px;
	}
	.modal .modal-footer {
		background: #ecf0f1;
		border-radius: 0 0 3px 3px;
	}
    .modal .modal-title {
        display: inline-block;
    }
	.modal .form-control {
		border-radius: 2px;
		box-shadow: none;
		border-color: #dddddd;
	}
	.modal textarea.form-control {
		resize: vertical;
	}
	.modal .btn {
		border-radius: 2px;
		min-width: 100px;
	}
	.modal form label {
		font-weight: normal;
	}
  .navbar {
    margin-top: 20px; background-color: white;
  }
  .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > li > a{
    font-size: 16px;
}
.font22{font-size: 22px !important;}
.mrgnlft5{margin-left: 5px !important;}
.mrgnrgt5{margin-right: 5px !important;}
.preload {
    width:100px;
    height: 100px;
    position: fixed;
    top: 50%;
    left: 50%;
        z-index: 99999;
}
.lds-roller {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-roller div {
  animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  transform-origin: 32px 32px;
}
.lds-roller div:after {
  content: " ";
  display: block;
  position: absolute;
  width: 6px;
  height: 6px;
  border-radius: 50%;
  background: #435d7d;
  margin: -3px 0 0 -3px;
}
.lds-roller div:nth-child(1) {
  animation-delay: -0.036s;
}
.lds-roller div:nth-child(1):after {
  top: 50px;
  left: 50px;
}
.lds-roller div:nth-child(2) {
  animation-delay: -0.072s;
}
.lds-roller div:nth-child(2):after {
  top: 54px;
  left: 45px;
}
.lds-roller div:nth-child(3) {
  animation-delay: -0.108s;
}
.lds-roller div:nth-child(3):after {
  top: 57px;
  left: 39px;
}
.lds-roller div:nth-child(4) {
  animation-delay: -0.144s;
}
.lds-roller div:nth-child(4):after {
  top: 58px;
  left: 32px;
}
.lds-roller div:nth-child(5) {
  animation-delay: -0.18s;
}
.lds-roller div:nth-child(5):after {
  top: 57px;
  left: 25px;
}
.lds-roller div:nth-child(6) {
  animation-delay: -0.216s;
}
.lds-roller div:nth-child(6):after {
  top: 54px;
  left: 19px;
}
.lds-roller div:nth-child(7) {
  animation-delay: -0.252s;
}
.lds-roller div:nth-child(7):after {
  top: 50px;
  left: 14px;
}
.lds-roller div:nth-child(8) {
  animation-delay: -0.288s;
}
.lds-roller div:nth-child(8):after {
  top: 45px;
  left: 10px;
}
@keyframes lds-roller {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

</style>

<script type="text/javascript">
$(window).load(function(){
   $('.preload').fadeOut();
});
$(document).ready(function(){

	// Activate tooltip
	$('[data-toggle="tooltip"]').tooltip();

	// Select/Deselect checkboxes
	var checkbox = $('table tbody input[type="checkbox"]');
	$("#selectAll").click(function(){
		if(this.checked){
			checkbox.each(function(){
				this.checked = true;
			});
		} else{
			checkbox.each(function(){
				this.checked = false;
			});
		}
	});
	checkbox.click(function(){
		if(!this.checked){
			$("#selectAll").prop("checked", false);
		}
	});
});
</script>
</head>
<body>

<div class="container">
    @include('elements.header')
        @yield('content')
</div>
	<!-- Edit Modal HTML -->
	<div id="addEventModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal" id="addEventForm">
					<div class="modal-header">
						<h4 class="modal-title">Add Event</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
            <div class="container-fluid">
              <meta name="csrf-token" content="{{ csrf_token() }}">

              <div class="row">
                <div class="col-md-12 errormessages">

                </div>
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>Event name</label>
      							<input type="text" class="form-control" name="name" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
      							<label>Intro</label>
      							<input type="text" class="form-control" name="intro" required>
      						</div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>Start</label>
      							<input type="date" class="form-control" name="start_time" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
      							<label>End</label>
      							<input type="date" class="form-control"  name="end_time" required>
      						</div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
                    <label>Show guests</label>
                    <select class="form-control" name="show_guests">
                    	<option value="true">Yes</option>
                    	<option value="false">No</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
                    <label>Capacity</label>
                    <input type="number" class="form-control"  name="capacity" required>
                  </div>
                </div>
              </div>

              <div class="row">
                  <div style="width: 100%; height: 12px; border-bottom: 1px solid black; text-align: center;margin-bottom: 18px;">
                    <span style="font-size: 15px; background-color: #F3F5F6; padding: 5px 10px;">
                      Contact details
                    </span>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>Contact name</label>
      							<input type="text" class="form-control" name="contact[name]" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
      							<label>Contact email</label>
      							<input type="text" class="form-control" name="contact[email]" required>
      						</div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>Contact Phone</label>
      							<input type="text" class="form-control" name="contact[contact_phone]" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <!-- <div class="form-group">
      							<label>Contact email</label>
      							<input type="text" class="form-control" required>
      						</div> -->
                </div>
              </div>

              <div class="row">
                  <div style="width: 100%; height: 12px; border-bottom: 1px solid black; text-align: center;margin-bottom: 18px;">
                    <span style="font-size: 15px; background-color: #F3F5F6; padding: 5px 10px;">
                      Venue details
                    </span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
                    <label>Venue name</label>
                    <input type="text" class="form-control" name="venue[name]" required>
                  </div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="form-control" name="venue[address][address1]" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
                    <label>City</label>
                    <input type="text" class="form-control" name="venue[address][city]" required>
                  </div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
                    <label>State</label>
                    <select class="form-control"  name="venue[address][state]">
                    	<option value="AL">Alabama</option>
                    	<option value="AK">Alaska</option>
                    	<option value="AZ">Arizona</option>
                    	<option value="AR">Arkansas</option>
                    	<option value="CA">California</option>
                    	<option value="CO">Colorado</option>
                    	<option value="CT">Connecticut</option>
                    	<option value="DE">Delaware</option>
                    	<option value="DC">District Of Columbia</option>
                    	<option value="FL">Florida</option>
                    	<option value="GA">Georgia</option>
                    	<option value="HI">Hawaii</option>
                    	<option value="ID">Idaho</option>
                    	<option value="IL">Illinois</option>
                    	<option value="IN">Indiana</option>
                    	<option value="IA">Iowa</option>
                    	<option value="KS">Kansas</option>
                    	<option value="KY">Kentucky</option>
                    	<option value="LA">Louisiana</option>
                    	<option value="ME">Maine</option>
                    	<option value="MD">Maryland</option>
                    	<option value="MA">Massachusetts</option>
                    	<option value="MI">Michigan</option>
                    	<option value="MN">Minnesota</option>
                    	<option value="MS">Mississippi</option>
                    	<option value="MO">Missouri</option>
                    	<option value="MT">Montana</option>
                    	<option value="NE">Nebraska</option>
                    	<option value="NV">Nevada</option>
                    	<option value="NH">New Hampshire</option>
                    	<option value="NJ">New Jersey</option>
                    	<option value="NM">New Mexico</option>
                    	<option value="NY">New York</option>
                    	<option value="NC">North Carolina</option>
                    	<option value="ND">North Dakota</option>
                    	<option value="OH">Ohio</option>
                    	<option value="OK">Oklahoma</option>
                    	<option value="OR">Oregon</option>
                    	<option value="PA">Pennsylvania</option>
                    	<option value="RI">Rhode Island</option>
                    	<option value="SC">South Carolina</option>
                    	<option value="SD">South Dakota</option>
                    	<option value="TN">Tennessee</option>
                    	<option value="TX">Texas</option>
                    	<option value="UT">Utah</option>
                    	<option value="VT">Vermont</option>
                    	<option value="VA">Virginia</option>
                    	<option value="WA">Washington</option>
                    	<option value="WV">West Virginia</option>
                    	<option value="WI">Wisconsin</option>
                    	<option value="WY">Wyoming</option>
                    </select>
                  </div>
                </div>
              </div>

            </div>


					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-success" value="Add">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Edit Modal HTML -->
	<div id="editEventModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form  class="form-horizontal" id="editEventForm">
					<div class="modal-header">
						<h4 class="modal-title">Edit Employee</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
            <div class="container-fluid">
              <meta name="csrf-token" content="{{ csrf_token() }}">
              <input type="hidden" class="form-control eventId" name="id" required>
              <div class="row">
                <div class="col-md-12 errormessages">

                </div>
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>Event name</label>
      							<input type="text" class="form-control" name="name" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
      							<label>Intro</label>
      							<input type="text" class="form-control" name="intro" required>
      						</div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>Start</label>
      							<input type="date" class="form-control" name="start_time" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
      							<label>End</label>
      							<input type="date" class="form-control"  name="end_time" required>
      						</div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
                    <label>Show guests</label>
                    <select class="form-control" name="show_guests">
                    	<option value="true">Yes</option>
                    	<option value="false">No</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
                    <label>Capacity</label>
                    <input type="number" class="form-control"  name="capacity" required>
                  </div>
                </div>
              </div>

              <div class="row">
                  <div style="width: 100%; height: 12px; border-bottom: 1px solid black; text-align: center;margin-bottom: 18px;">
                    <span style="font-size: 15px; background-color: #F3F5F6; padding: 5px 10px;">
                      Contact details
                    </span>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>Contact name</label>
      							<input type="text" class="form-control contactName" name="contact[name]" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
      							<label>Contact email</label>
      							<input type="text" class="form-control contactEmail" name="contact[email]" required>
      						</div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>Contact Phone</label>
      							<input type="text" class="form-control contactPhone" name="contact[contact_phone]" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <!-- <div class="form-group">
      							<label>Contact email</label>
      							<input type="text" class="form-control" required>
      						</div> -->
                </div>
              </div>

              <div class="row">
                  <div style="width: 100%; height: 12px; border-bottom: 1px solid black; text-align: center;margin-bottom: 18px;">
                    <span style="font-size: 15px; background-color: #F3F5F6; padding: 5px 10px;">
                      Venue details
                    </span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
                    <label>Venue name</label>
                    <input type="text" class="form-control venueName" name="venue[name]" required>
                  </div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="form-control venueAddressAddress1" name="venue[address][address1]" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
                    <label>City</label>
                    <input type="text" class="form-control venueAddressCity" name="venue[address][city]" required>
                  </div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
                    <label>State</label>
                    <select class="form-control venueAddressState"  name="venue[address][state]">
                    	<option value="AL">Alabama</option>
                    	<option value="AK">Alaska</option>
                    	<option value="AZ">Arizona</option>
                    	<option value="AR">Arkansas</option>
                    	<option value="CA">California</option>
                    	<option value="CO">Colorado</option>
                    	<option value="CT">Connecticut</option>
                    	<option value="DE">Delaware</option>
                    	<option value="DC">District Of Columbia</option>
                    	<option value="FL">Florida</option>
                    	<option value="GA">Georgia</option>
                    	<option value="HI">Hawaii</option>
                    	<option value="ID">Idaho</option>
                    	<option value="IL">Illinois</option>
                    	<option value="IN">Indiana</option>
                    	<option value="IA">Iowa</option>
                    	<option value="KS">Kansas</option>
                    	<option value="KY">Kentucky</option>
                    	<option value="LA">Louisiana</option>
                    	<option value="ME">Maine</option>
                    	<option value="MD">Maryland</option>
                    	<option value="MA">Massachusetts</option>
                    	<option value="MI">Michigan</option>
                    	<option value="MN">Minnesota</option>
                    	<option value="MS">Mississippi</option>
                    	<option value="MO">Missouri</option>
                    	<option value="MT">Montana</option>
                    	<option value="NE">Nebraska</option>
                    	<option value="NV">Nevada</option>
                    	<option value="NH">New Hampshire</option>
                    	<option value="NJ">New Jersey</option>
                    	<option value="NM">New Mexico</option>
                    	<option value="NY">New York</option>
                    	<option value="NC">North Carolina</option>
                    	<option value="ND">North Dakota</option>
                    	<option value="OH">Ohio</option>
                    	<option value="OK">Oklahoma</option>
                    	<option value="OR">Oregon</option>
                    	<option value="PA">Pennsylvania</option>
                    	<option value="RI">Rhode Island</option>
                    	<option value="SC">South Carolina</option>
                    	<option value="SD">South Dakota</option>
                    	<option value="TN">Tennessee</option>
                    	<option value="TX">Texas</option>
                    	<option value="UT">Utah</option>
                    	<option value="VT">Vermont</option>
                    	<option value="VA">Virginia</option>
                    	<option value="WA">Washington</option>
                    	<option value="WV">West Virginia</option>
                    	<option value="WI">Wisconsin</option>
                    	<option value="WY">Wyoming</option>
                    </select>
                  </div>
                </div>
              </div>

            </div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-info" value="Save">
					</div>
				</form>
			</div>
		</div>
	</div>

  <!-- Edit Modal HTML -->
	<div id="addPeopleModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal" id="addPeopleForm">
					<div class="modal-header">
						<h4 class="modal-title">Add People</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
            <div class="container-fluid">
              <meta name="csrf-token" content="{{ csrf_token() }}">

              <div class="row">
                <div class="col-md-12 errormessages">

                </div>
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>First name</label>
      							<input type="text" class="form-control" name="first_name" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
      							<label>Last name</label>
      							<input type="text" class="form-control" name="last_name" required>
      						</div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>Email</label>
      							<input type="email" class="form-control" name="email" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
                    <label>Gender</label>
                    <select class="form-control" name="sex">
                    	<option value="M">Male</option>
                    	<option value="F">Female</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
                    <label>Employer</label>
                    <input type="text" class="form-control"  name="employer" required>
                  </div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
                    <label>State</label>
                    <select class="form-control"  name="registered_address[state]">
                    	<option value="AL">Alabama</option>
                    	<option value="AK">Alaska</option>
                    	<option value="AZ">Arizona</option>
                    	<option value="AR">Arkansas</option>
                    	<option value="CA">California</option>
                    	<option value="CO">Colorado</option>
                    	<option value="CT">Connecticut</option>
                    	<option value="DE">Delaware</option>
                    	<option value="DC">District Of Columbia</option>
                    	<option value="FL">Florida</option>
                    	<option value="GA">Georgia</option>
                    	<option value="HI">Hawaii</option>
                    	<option value="ID">Idaho</option>
                    	<option value="IL">Illinois</option>
                    	<option value="IN">Indiana</option>
                    	<option value="IA">Iowa</option>
                    	<option value="KS">Kansas</option>
                    	<option value="KY">Kentucky</option>
                    	<option value="LA">Louisiana</option>
                    	<option value="ME">Maine</option>
                    	<option value="MD">Maryland</option>
                    	<option value="MA">Massachusetts</option>
                    	<option value="MI">Michigan</option>
                    	<option value="MN">Minnesota</option>
                    	<option value="MS">Mississippi</option>
                    	<option value="MO">Missouri</option>
                    	<option value="MT">Montana</option>
                    	<option value="NE">Nebraska</option>
                    	<option value="NV">Nevada</option>
                    	<option value="NH">New Hampshire</option>
                    	<option value="NJ">New Jersey</option>
                    	<option value="NM">New Mexico</option>
                    	<option value="NY">New York</option>
                    	<option value="NC">North Carolina</option>
                    	<option value="ND">North Dakota</option>
                    	<option value="OH">Ohio</option>
                    	<option value="OK">Oklahoma</option>
                    	<option value="OR">Oregon</option>
                    	<option value="PA">Pennsylvania</option>
                    	<option value="RI">Rhode Island</option>
                    	<option value="SC">South Carolina</option>
                    	<option value="SD">South Dakota</option>
                    	<option value="TN">Tennessee</option>
                    	<option value="TX">Texas</option>
                    	<option value="UT">Utah</option>
                    	<option value="VT">Vermont</option>
                    	<option value="VA">Virginia</option>
                    	<option value="WA">Washington</option>
                    	<option value="WV">West Virginia</option>
                    	<option value="WI">Wisconsin</option>
                    	<option value="WY">Wyoming</option>
                    </select>
                  </div>
                </div>
              </div>

            </div>

					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-success" value="Add">
					</div>
				</form>
			</div>
		</div>
	</div>

  <!-- Edit Modal HTML -->
	<div id="editPeopleModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal" id="editPeopleForm">
					<div class="modal-header">
						<h4 class="modal-title">Edit People</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
            <div class="container-fluid">
              <meta name="csrf-token" content="{{ csrf_token() }}">
              <input type="hidden" class="form-control peopleId" name="id" required>
              <div class="row">
                <div class="col-md-12 errormessages">

                </div>
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>First name</label>
      							<input type="text" class="form-control firstName" name="first_name" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
      							<label>Last name</label>
      							<input type="text" class="form-control lastName" name="last_name" required>
      						</div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
      							<label>Email</label>
      							<input type="email" class="form-control email" name="email" required>
      						</div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
                    <label>Gender</label>
                    <select class="form-control sex" name="sex">
                    	<option value="M">Male</option>
                    	<option value="F">Female</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 ">
                  <div class="form-group mrgnrgt5">
                    <label>Employer</label>
                    <input type="text" class="form-control employer"  name="employer" required>
                  </div>
                </div>
                <div class="col-md-6 ml-auto">
                  <div class="form-group">
                    <label>State</label>
                    <select class="form-control registeredAddressState"  name="registered_address[state]">
                    	<option value="AL">Alabama</option>
                    	<option value="AK">Alaska</option>
                    	<option value="AZ">Arizona</option>
                    	<option value="AR">Arkansas</option>
                    	<option value="CA">California</option>
                    	<option value="CO">Colorado</option>
                    	<option value="CT">Connecticut</option>
                    	<option value="DE">Delaware</option>
                    	<option value="DC">District Of Columbia</option>
                    	<option value="FL">Florida</option>
                    	<option value="GA">Georgia</option>
                    	<option value="HI">Hawaii</option>
                    	<option value="ID">Idaho</option>
                    	<option value="IL">Illinois</option>
                    	<option value="IN">Indiana</option>
                    	<option value="IA">Iowa</option>
                    	<option value="KS">Kansas</option>
                    	<option value="KY">Kentucky</option>
                    	<option value="LA">Louisiana</option>
                    	<option value="ME">Maine</option>
                    	<option value="MD">Maryland</option>
                    	<option value="MA">Massachusetts</option>
                    	<option value="MI">Michigan</option>
                    	<option value="MN">Minnesota</option>
                    	<option value="MS">Mississippi</option>
                    	<option value="MO">Missouri</option>
                    	<option value="MT">Montana</option>
                    	<option value="NE">Nebraska</option>
                    	<option value="NV">Nevada</option>
                    	<option value="NH">New Hampshire</option>
                    	<option value="NJ">New Jersey</option>
                    	<option value="NM">New Mexico</option>
                    	<option value="NY">New York</option>
                    	<option value="NC">North Carolina</option>
                    	<option value="ND">North Dakota</option>
                    	<option value="OH">Ohio</option>
                    	<option value="OK">Oklahoma</option>
                    	<option value="OR">Oregon</option>
                    	<option value="PA">Pennsylvania</option>
                    	<option value="RI">Rhode Island</option>
                    	<option value="SC">South Carolina</option>
                    	<option value="SD">South Dakota</option>
                    	<option value="TN">Tennessee</option>
                    	<option value="TX">Texas</option>
                    	<option value="UT">Utah</option>
                    	<option value="VT">Vermont</option>
                    	<option value="VA">Virginia</option>
                    	<option value="WA">Washington</option>
                    	<option value="WV">West Virginia</option>
                    	<option value="WI">Wisconsin</option>
                    	<option value="WY">Wyoming</option>
                    </select>
                  </div>
                </div>
              </div>

            </div>

					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-success" value="Save">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Delete Modal HTML -->
	<div id="deletePeopleModal" class="modal fade">
		<div class="modal-dialog" style="    top: 25%;">
			<div class="modal-content">
				<form class="form-horizontal" id="deletePeopleForm">
          <meta name="csrf-token" content="{{ csrf_token() }}">
          <input type="hidden" class="form-control peopleId" name="id" required>
					<div class="modal-header">
						<h4 class="modal-title">Delete People</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<p>Are you sure you want to delete this Record?</p>
						<p class="text-warning"><small>This action cannot be undone.</small></p>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-danger" value="Delete">
					</div>
				</form>
			</div>
		</div>
	</div>
  <div class="preload">
    <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
<!-- <img src="http://i.imgur.com/KUJoe.gif"> -->
</div>
</body>
</html>
