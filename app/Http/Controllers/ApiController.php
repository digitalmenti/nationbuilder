<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper;
class ApiController extends Controller
{
    public function eventslist(){
      $helper = new Helper;
      $data = $helper->callAPI('GET','sites/digitalmenti/pages/events','');
      $data = json_decode($data, true);
      return view('events.index', compact('data'));
    }

    public function eventscreate(Request $request){
      $inputdata = $request->all();
      $inputdata['status'] = 'unlisted';
      $inputdata['time_zone'] = "Pacific Time (US & Canada)";
      $inputdata['contact']['show_phone'] = true;
      $inputdata['contact']['show_email'] = true;
      $inputdata['contact']['contact_email'] = $inputdata['contact']['email'];
      $inputdata['contact']['contact_phone'] = $inputdata['contact']['contact_phone'];
      $inputdata['rsvp_form']['phone'] = 'optional';
      $inputdata['rsvp_form']['address'] = 'required';
      $inputdata['rsvp_form']['allow_guests'] = true;
      $inputdata['rsvp_form']['accept_rsvps'] = true;
      $inputdata['rsvp_form']['gather_volunteers'] = true;
      $data['event'] = $inputdata;
      $data = json_encode($data, true);
      $helper = new Helper;
      $response = $helper->callAPI('POST','sites/digitalmenti/pages/events',$data);
      $response = json_decode($response, true);
      if(!empty($response['event'])){
        return response()->json([
            'success' => true,
            'message' => 'Event added successfully.',
            'data' => $response['event']
        ]);
      }else{
        return response()->json([
            'success' => false,
            'message' => !empty($response['message'])?$response['message']:"Something went wrong.",
            'data' => !empty($response['validation_errors'])?$response['validation_errors']:""
        ]);
      }
    }

    public function eventsedit(Request $request){
      $inputdata = $request->all();
      $inputdata['status'] = 'unlisted';
      $inputdata['time_zone'] = "Pacific Time (US & Canada)";
      $inputdata['contact']['show_phone'] = true;
      $inputdata['contact']['show_email'] = true;
      $inputdata['contact']['contact_email'] = $inputdata['contact']['email'];
      $inputdata['contact']['phone'] = $inputdata['contact']['contact_phone'];
      $inputdata['rsvp_form']['phone'] = 'optional';
      $inputdata['rsvp_form']['address'] = 'required';
      $inputdata['rsvp_form']['allow_guests'] = true;
      $inputdata['rsvp_form']['accept_rsvps'] = true;
      $inputdata['rsvp_form']['gather_volunteers'] = true;
      $data['event'] = $inputdata;
      // print_r($inputdata);die;
      $data = json_encode($data, true);
      $helper = new Helper;
      $response = $helper->callAPI('PUT','sites/digitalmenti/pages/events/'.$inputdata['id'],$data);
      $response = json_decode($response, true);
      if(!empty($response['event'])){
        return response()->json([
            'success' => true,
            'message' => 'Event updated successfully.',
            'data' => $response['event']
        ]);
      }else{
        return response()->json([
            'success' => false,
            'message' => !empty($response['message'])?$response['message']:"Something went wrong.",
            'data' => !empty($response['validation_errors'])?$response['validation_errors']:""
        ]);
      }
    }

    /* people apis */

    public function peoplelist(Request $request){
      $helper = new Helper;
      $data = $helper->callAPI('GET','people','');
      $data = json_decode($data, true);
      return view('people.index', compact('data'));
    }

    public function peoplecreate(Request $request){
      $inputdata = $request->all();
      $inputdata['signup_type'] = 0;
      $inputdata['party'] = "P";
      $inputdata['registered_address']['country_code'] = "US";
      $data['person'] = $inputdata;
      $data = json_encode($data, true);
      $helper = new Helper;
      $response = $helper->callAPI('POST','people',$data);
      $response = json_decode($response, true);
      if(!empty($response['person'])){
        return response()->json([
            'success' => true,
            'message' => 'Person added successfully.',
            'data' => $response['person']
        ]);
      }else{
        return response()->json([
            'success' => false,
            'message' => !empty($response['message'])?$response['message']:"Something went wrong.",
            'data' => !empty($response['validation_errors'])?$response['validation_errors']:""
        ]);
      }
    }

    public function peopleedit(Request $request){
      $inputdata = $request->all();
      $inputdata['signup_type'] = 0;
      $inputdata['party'] = "P";
      $inputdata['registered_address']['country_code'] = "US";
      $data['person'] = $inputdata;
      $data = json_encode($data, true);
      $helper = new Helper;
      $response = $helper->callAPI('PUT','people/'.$inputdata['id'],$data);
      $response = json_decode($response, true);
      if(!empty($response['person'])){
        return response()->json([
            'success' => true,
            'message' => 'Person updated successfully.',
            'data' => $response['person']
        ]);
      }else{
        return response()->json([
            'success' => false,
            'message' => !empty($response['message'])?$response['message']:"Something went wrong.",
            'data' => !empty($response['validation_errors'])?$response['validation_errors']:""
        ]);
      }
    }


        public function peopledelete(Request $request){
          $inputdata = $request->all();
          $helper = new Helper;
          $response = $helper->callAPI("DELETE",'people/'.$inputdata['id'],'');
          $response = json_decode($response, true);
          return response()->json([
              'success' => true,
              'message' => 'Person deleted successfully.',
              'data' => $response['person']
          ]);
        }

}
